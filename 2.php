<?php

// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases2/' . $clase . '.php';
});

$caja1=new Caja("negro", 2, 3, 5, 25, true);

var_dump($caja1);

$armario1=new Armario("negro", 2, 3, 5, 100, true, true, 5);

var_dump($armario1);

$habitacion1=new Habitacion($armario1, 10, 20, "azul");

var_dump($habitacion1);