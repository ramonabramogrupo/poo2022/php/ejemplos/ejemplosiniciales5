<?php

// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

// crear un perro

$perro1=new Perro("tintin", 10, "gris", true, new Persona("Rosa", "Calle Uno", "623623623"));

// mostrar el nombre del perro

echo "<br>";
echo $perro1->nombre;  // tintin

// mostrar el nombre del dueño del perro

echo "<br>";
echo $perro1->persona->nombre;  // Rosa


// creamos otro perrito

$perro2=new Perro("Luisa", 15, "negro", false, new Persona("Jose","Calle Luis","654654654"));

// coloco como dueño el mismo que el del perro2
$perro3=new Perro("Luis", 15, "negro", false, $perro2->persona);

// al cambiar el nombre del dueño del perro3 cambio el nombre del dueño del perro2
$perro3->persona->nombre="Jorge";

var_dump($perro3,$perro2);


$tor=new Vaca("Tor", 10, "blanca", "valle a", "los felipes");

var_dump($tor);

echo $tor->getNombre(); // accedemos al nombre de la vaca con el getter de animal (superclase)