<?php

class Caja{
    public $nombre;
    public function __construct($nombre) {
        $this->nombre=$nombre;
    }
}

$objeto=new Caja("uno"); // objeto de tipo Caja con nombre uno

$b="uno"; // dato primitivo (texto) cuyo valor es uno

// con datos primitivos el operador de "=" te crea copias (clon)

$copia=$b; // asigno el valor de b a copia

$b="dos"; // al cambiar b no cambia la copia

echo $copia; // uno

// con esto no creo una copia de un objeto "="
$copiaObjeto=$objeto; // asignado una referencia al objeto "objeto" llamado copiaObjeto

$objeto->nombre="dos"; 

echo $copiaObjeto->nombre; // dos

// utilizando la palabra reservada clone te crea una copia del objeto
$clonObjeto=clone $objeto;

$objeto->nombre="nuevo";

echo $clonObjeto->nombre;  // dos








