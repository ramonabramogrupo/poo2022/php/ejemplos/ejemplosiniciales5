<?php

/**
 * Description of Vaca
 *
 * @author Profesor Ramon
 */
class Vaca extends Animal{
    public $direccion;
    public $granja;
    
    public function __construct($nombre, $peso, $color, $direccion, $granja) {
        $this->nombre = $nombre;
        $this->peso = $peso;
        $this->color = $color;
        $this->direccion = $direccion;
        $this->granja = $granja;
    }

}



