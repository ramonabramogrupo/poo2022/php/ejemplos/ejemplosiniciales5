<?php

/**
 * Clase Animal
 *
 * @author Profesor Ramon
 */
class Animal {
   public $nombre;
   public $peso;
   public $color;
      
   public function getNombre() {
       return $this->nombre;
   }

   public function getPeso() {
       return $this->peso;
   }

   public function getColor() {
       return $this->color;
   }

   public function setNombre($nombre): void {
       $this->nombre = $nombre;
   }

   public function setPeso($peso): void {
       $this->peso = $peso;
   }

   public function setColor($color): void {
       $this->color = $color;
   }



}
