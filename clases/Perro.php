<?php

/**
 * Description of Perro
 *
 * @author Profesor Ramon
 */
class Perro extends Animal{
    public $vacunado;
    public $persona;
    
    public function __construct($nombre, $peso, $color, $vacunado, Persona $persona) {
        $this->nombre = $nombre;
        $this->peso = $peso;
        $this->color = $color;
        $this->vacunado = $vacunado;
        $this->persona = $persona;
    }

    
    
}
