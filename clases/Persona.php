<?php

/**
 * Description of Persona
 *
 * @author Profesor Ramon
 */
class Persona {
    public $nombre;
    public $direccion;
    public $telefono;
    
    public function __construct($nombre, $direccion, $telefono) {
        $this->nombre = $nombre;
        $this->direccion = $direccion;
        $this->telefono = $telefono;
    }

}
