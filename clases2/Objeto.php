<?php

/**
 * Description of Objeto
 *
 * @author Profesor Ramon
 */
class Objeto {
    public $color;
    public $alto;
    public $ancho;
    public $fondo;
        
    public function __construct($color, $alto, $ancho, $fondo) {
        $this->color = $color;
        $this->alto = $alto;
        $this->ancho = $ancho;
        $this->fondo = $fondo;
    }

}
