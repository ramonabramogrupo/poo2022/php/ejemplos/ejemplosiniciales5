<?php

/**
 * Description of Habitacion
 *
 * @author Profesor Ramon
 */
class Habitacion {
    public $armario;
    public $ancho;
    public $largo;
    public $color;
    
    public function __construct(Armario $armario, int $ancho, $largo, $color) {
        $this->armario = $armario;
        $this->ancho = $ancho;
        $this->largo = $largo;
        $this->color = $color;
    }

}

