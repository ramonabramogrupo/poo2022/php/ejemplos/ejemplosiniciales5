<?php

/**
 * Description of Armario
 *
 * @author Profesor Ramon
 */
class Armario extends Objeto{
    public $peso;
    public $puerta;
    public $estanterias;
    public $numeroCajones;
    
    public function __construct($color, $alto, $ancho, $fondo,$peso, $puerta, $estanterias, $numeroCajones) {
        $this->peso = $peso;
        $this->puerta = $puerta;
        $this->estanterias = $estanterias;
        $this->numeroCajones = $numeroCajones;
        $this->color = $color;
        $this->alto = $alto;
        $this->ancho = $ancho;
        $this->fondo = $fondo;
    }

    
}
