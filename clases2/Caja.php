<?php

/**
 * Description of Caja
 *
 * @author Profesor Ramon
 */
class Caja extends Objeto{
    public $volumen;
    public $cierre;
    
    public function __construct($color, $alto, $ancho, $fondo,$volumen, $cierre) {
        $this->volumen = $volumen;
        $this->cierre = $cierre;
        $this->color = $color;
        $this->alto = $alto;
        $this->ancho = $ancho;
        $this->fondo = $fondo;
    }

}
